# Comparison PDFs

>**Notes:**
- Before creating the PDF for a new comparison page, you need to have
  [created the page](https://about.gitlab.com/handbook/marketing/website/#creating-a-comparison-page)
  beforehand.
- There is currently a bug and the PDFs look different when created on Linux
  than on macOS. See https://gitlab.com/gitlab-com/www-gitlab-com/issues/1356.

The comparison PDFs are created using an HTML template and they are included
in the repository.

You will need to install [wkhtmltopdf](https://wkhtmltopdf.org/downloads.html).

On macOS:

```
brew cask install wkhtmltopdf
```

The comparison PDFs are generated in a slightly different way and require a
different command to be run.

1. Before the PDFs can be generated the website needs to be built locally:

    ```sh
    bundle exec middleman build
    ```

1. After running that you can now run the following to generate the PDFs:

    ```sh
    bundle exec rake comparison_pdfs
    ```

1. Once you have done that you are free to commit and push these to GitLab.com:

    ```sh
    git add source/devops-tools/pdfs/
    git commit -m "Update comparison PDFs"
    git push origin branch_name
    ```
